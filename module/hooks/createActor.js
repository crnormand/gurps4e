/**
 * Set default values for new actors
 */

Hooks.on('createActor', async (actor, options, userId) => {
  if (actor.data.type === 'minchar' && options.renderSheet) {

    // Use a script to create the essential items for a character.
    var scriptdata = [
      {
        "name": "Strength",
        "type": "Primary-Attribute",
        "data": {
          "notes": "Your Strength attribute.",
          "abbr": "ST",
          "value": 10
        }
      },
      {
        "name": "Dexterity",
        "type": "Primary-Attribute",
        "data": {
          "notes": "Your Dexterity attribute.",
          "abbr": "DX",
          "value": 10
        }
      },
      {
        "name": "Intelligence",
        "type": "Primary-Attribute",
        "data": {
          "notes": "Your Intelligence attribute.",
          "abbr": "IQ",
          "value": 10
        }
      },
      {
        "name": "Health",
        "type": "Primary-Attribute",
        "data": {
          "notes": "Your Health attribute.",
          "abbr": "HT",
          "value": 10
        }
      },
      {
        "name": "Perception",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to notice things.",
          "value": 10,
          "formula": "@iq",
          "category": "check"
        }
      },
      {
        "name": "Will",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to resist being dominated mentally.",
          "value": 10,
          "formula": "@iq",
          "category": "check"
        }
      },
      {
        "name": "Hit Points",
        "type": "Pool",
        "data": {
          "notes": "The representation of the amount of damage you can sustain.",
          "inHeader": true,
          "abbr": "HP",
          "name": "Hit Points",
          "state": "[H]",
          "min": -50,
          "value": 10,
          "max": 10
        }
      },
      {
        "name": "Fatigue Points",
        "type": "Pool",
        "data": {
          "notes": "The representation of the amount of endurance you possess.",
          "inHeader": true,
          "abbr": "FP",
          "name": "Fatigue Points",
          "state": "[F]",
          "min": -10,
          "value": 10,
          "max": 10
        }
      },
      {
        "name": "Control Points",
        "type": "Pool",
        "data": {
          "notes": "The representation of the amount of physical constraint you can resist.",
          "inHeader": true,
          "abbr": "CP",
          "name": "Control Points",
          "state": "",
          "min": "0",
          "value": 0,
          "max": 10
        }
      },
      {
        "name": "Energy Reserve",
        "type": "Pool",
        "data": {
          "notes": "The representation of the amount of external spell-casting potential you possess.",
          "inHeader": true,
          "abbr": "ER",
          "name": "Energy Reserve",
          "state": "",
          "min": "0",
          "value": 0,
          "max": 0
        }
      },
      {
        "name": "Reaction",
        "type": "Rollable",
        "data": {
          "notes": "A Reaction roll",
          "value": 0,
          "formula": "0",
          "category": "check"
        }
      },
      {
        "name": "Dodge",
        "type": "Defence",
        "data": {
          "notes": "Your ability to Dodge attacks.",
          "value": 10,
          "formula": "(@ht +@dx)/4+3",
          "category": "dodge",
          "weight": 0
        }
      },
      {
        "name": "Knockdown & Stun",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to resist being subject to Knockdown & Stun.",
          "value": 10,
          "formula": "@ht",
          "category": "check"
        }
      },
      {
        "name": "Consciousness",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to remain conscious despite your wounds.",
          "value": 10,
          "formula": "@ht",
          "category": "check"
        }
      },
      {
        "name": "Death",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to avoid dying despite your wounds.",
          "value": 10,
          "formula": "@ht",
          "category": "check"
        }
      },
      {
        "name": "Full Body",
        "type": "Hit-Location",
        "data": {
          "notes": "note",
          "damageResistance": "0",
          "damage": "0",
          "injuryType": "cr",
          "toCripple": "N/A",
          "crippled": false,
          "toHitRoll18": "All",
          "toHitRoll6": "All",
          "toHitPenalty": "0"
        }
      },
      {
        "name": "Fright",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to resist fear.",
          "value": 10,
          "formula": "@iq",
          "category": "check"
        }
      },
      {
        "name": "Mental Stun Recovery",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to recover from being mentally stunned.",
          "value": 10,
          "formula": "@iq",
          "category": "check"
        }
      },
      {
        "name": "Physical Stun Recovery",
        "type": "Rollable",
        "data": {
          "notes": "Your ability to recover from being physically stunned.",
          "value": 10,
          "formula": "@ht",
          "category": "check"
        }
      }
    ];
    for (var item of scriptdata) {
      await actor.createOwnedItem(item);
    }
  }
});