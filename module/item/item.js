/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class gurpsItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   * 
   * Note: This does not appear to do anything that is not already accomplished
   * by template.json but may be part of the functionality required for dynamic 
   * updating of items inline. TBC
   * 
   * Note: This method is called on every item belonging to the actor, not just
   * the one being edited. So, if all the prepareItem methods are pointless we
   * should eliminate them.
   * 
   * item includes:
   *  _id:""
   *  name:""
   *  type:""
   *  img:""
   *  data:{} - which is specific to a type as defined in template.json
   *    notes:"" - all types have one
   */
  prepareData() {
    super.prepareData();

    return;

    // Get the Item's data
    let item = this.data;
    let data = item.data;

    // all types have one. Might as well update it here
    item.name = this.data && this.data.name ? this.data.name : "NewItem";
    data.notes = data.notes || "note";

    switch (item.type) {
      case "Item": // legacy item type
        data.quantity = data.quantity || 1;
        data.weight = data.weight || 0;
        break;
      case "Equipment":
        data.quantity = data.quantity || 1;
        data.weight = data.weight || 0;
        data.cost = data.cost || "";
        break;
      case "Hit-Location":
        data.damageResistance = data.damageResistance || 0;
        data.toCripple = data.toCripple || "";
        data.crippled = data.crippled || false;
        data.toHitRoll18 = data.toHitRoll18 || "13,14";
        data.toHitRoll6 = data.toHitRoll6 || "2";
        data.toHitPenalty = data.toHitPenalty || 0;
        break;
      case "Melee-Attack":
        data.level = data.level || 10;
        data.damage = data.damage || "1d6";
        data.damageType = data.damageType || "cut";
        data.armourDiv = data.armourDiv || 1;
        data.minST = data.minST || "8";
        data.weight = data.weight || 0;
        data.reach = data.reach || "1";
        break;
      case "Ranged-Attack":
        data.level = data.level || 10;
        data.damage = data.damage || "1d6";
        data.damageType = data.damageType || "cut";
        data.armourDiv = data.armourDiv || 1;
        data.minST = data.minST || "8";
        data.accuracy = data.accuracy || "1";
        data.range = data.range || "10/15";
        data.rof = data.rof || 1;
        data.shots = data.shots || "";
        data.bulk = data.bulk || -4;
        data.recoil = data.recoil || "";
        break;
      case "Rollable":
        data.level = data.level || 10;
        data.category = data.category || "";
        break;
      case "Modifier":
        data.inEffect = data.inEffect || false;
        data.modifier = data.modifier || 0;
        data.attack = data.attack || false;
        data.defence = data.defence || false;
        data.success = data.success || false;
        data.reaction = data.reaction || false;
        data.damage = data.damage || false;
        data.specific = data.specific || false;
        data.targets = data.targets || "";
        break;
      case "Defence":
        data.level = data.level || 10;
        data.category = data.category || "";
        data.weight = data.weight || 0;
        break;
      case "Trait":
        data.category = data.category || "";
        break;
      default: // not a supported type
        return ui.notifications.error(game.i18n.localize("GURPS4E.Error.BadItemtype"));
    }
  }

  // this does something I don't understand yet. Leaving it orphaned for now.
  _prepareDefenceData(itemData, data) {
    data.defences = Object.entries(GURPS4E.defences).map(entry => {
      const [category, label] = entry;
      return {
        category,
        label,
        checked: category === data.category,
      }
    });
  }
}
